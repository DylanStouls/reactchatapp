import * as React from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import { Router, Stack, Scene } from 'react-native-router-flux';
import { Home, Chat } from './components';
import { Provider } from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';
import rootReducer from './reducers';

const store = createStore(rootReducer, applyMiddleware(thunkMiddleware));


export default class App extends React.Component {

  render() {
    return (
      <Provider store={store}>
        <Router navigationBarStyle={styles.statusBar} titleStyle={styles.statusBar}>
          <Stack key="root">
            <Scene key="home" component={Home} title="Home" />
            <Scene key="chat" component={Chat} />
          </Stack>
        </Router>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  statusBar: {
    backgroundColor: 'orange',
    color: 'white'
  }
});
