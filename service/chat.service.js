import PouchDB from 'pouchdb-react-native';

class Chat {
    user= ''
    room=''
    db=null

    join(user, room) {
        this.user = user || 'Anonymous';
        this.room = (room || 'general')
        .normalize("NFD")
        .replace(/[\u0300-\u036f]/g, "");

        this.db = new PouchDB(this.room);

        return this.db.allDocs({
            include_docs : true
        });

    }
}

export const chatService = new Chat();