import { chatService } from '../service/chat.service';
import { Actions } from 'react-native-router-flux';

export const chatActions = { join }

function join(user, room) { 

    return dispach => {
        dispach({ type: 'JOIN_REQUEST', user, room });
        Actions.chat({user, title: `Room: ${room}`});
        chatService.join(user, room).then((messages) => dispach({ type: 'JOIN_SUCCESS', messages }));
    }
/* 
    function request(user, room) { return { type: 'JOIN_REQUEST', user, room } } 
    function success(messages) { return { type: 'JOIN_SUCCESS', messages } } 
    function failure(error) { return { type: 'FAILURE', error } } */
}