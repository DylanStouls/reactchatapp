import { combineReducers } from 'redux'; 
import chat from './chat.reducers';

export default combineReducers({
  chat
})