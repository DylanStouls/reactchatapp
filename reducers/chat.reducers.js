const initState = { 
    messages : [
        {_id:1, author:"Bob", content: "Hello Michel!", created_at: new Date()},
        {_id:2, author:"Michel", content: "Hello Bob!", created_at: new Date()},
        {_id:3, author:"Bob", content: "JEEE SUISSSSSS BOBOBOBOBOBOBOBOOBOBBO !!!!!", created_at: new Date()},
    ], 
    user: null, 
    room: null, 
}

export default function(state=initState, action) {
    console.log(action); 
    switch(action.type) {
        case 'JOIN_REQUEST': 
            return {...state, room: action.room, user: action.user};
        case 'JOIN_SUCCESS': 
            return {...state, messages: actions.messages};
        case 'FAILURE': console.error(action.error);
        default:
            return state;
    } 
}